public class Boss {
    public boolean life = true;
    public double tx, ty, size;
    public int timer = 0;
    public int lifeline = 5;
    public int lifeline0 = 5;
    public int index;
    public Boss(double tx0, double ty0, double size0, int i)
    {
        tx = tx0;
        ty = ty0;
        size = size0;
        index = i;
//        lframe = gframe0;
    }
    public void shot() // Imagine Dragon Respect!
    {
        if (Main.botbullets[index]==null) {
            Main.botbullets[index] = new Bullet1(tx, ty, .08);
            return;
        }
//        else if (Main.botbullets[index].life)
//        {
//            Main.botbullets[index].nextMove();
//            return;
//        }
        else if (!Main.botbullets[index].life) Main.botbullets[index] = new Bullet1(tx, ty, .08);
    }
    public void moveTo(double x, double y)
    {
        tx = x;
        ty = y;
    }
    public void display()
    {
        if (!life)
        {
//            if (cnt)
            return;
        }
        else
        {
//            StdDraw.setPenColor(StdDraw.BLUE);
//            StdDraw.filledCircle(tx, ty, size);
            StdDraw.picture(tx, ty, "LJpng.png", 0.2048*2, 0.1764*2);
        }
    }
    public void displayR()
    {
        if (!life)
        {
//            if (cnt)
            return;
        }
        else
        {
//            StdDraw.setPenColor(StdDraw.BLUE);
//            StdDraw.filledCircle(tx, ty, size);
            StdDraw.picture(tx, ty, "fuyouR.png", 0.2048*2, 0.1764*2);
        }
    }
    public void displayL()
    {
        if (!life)
        {
//            if (cnt)
            return;
        }
        else
        {
//            StdDraw.setPenColor(StdDraw.BLUE);
//            StdDraw.filledCircle(tx, ty, size);
            StdDraw.picture(tx, ty, "fuyouL.png", 0.2048*2, 0.1764*2);
        }
    }
    public void nextMove(Bullet[] bulletarr, Laser biu, Grader score)
    {
        if (life) {
            timer++;
//            shot();
            if (StdDraw.isKeyPressed(32) && biu.life) Elimination(biu, score);
            for (int j = 0; j < bulletarr.length; j++) {
                if (bulletarr[j] != null) Collition(bulletarr[j], score);
            }
        }
        else return;
    }
    public boolean isDead()
    {
        return (lifeline<=0);
    }
    public void dead()
    {
        life = false;
        if (Main.botbullets[index] != null) Main.botbullets[index].dead();
        StdAudio.playInBackground("ExpAu.wav");
        // StdDraw.setPenColor(StdDraw.BLUE);
        // StdDraw.text(tx, ty, "+ "+ score0);
        tx = 3.0;
        ty = -3.0;
    }
    public void hit(int i)
    {
        lifeline-=i;
        return;
    }
    public double whereX()
    {
        return tx;
    }
    public double whereY()
    {
        return ty;
    }
    public void Collition(Bullet B, Grader score) // Actually I should read radius also, but I am too lazy =-)
    {
        double x1 = tx;
        double y1 = ty;
        double x2 = B.whereX();
        double y2 = B.whereY();
        double radius = 0.04; // 0.2*0.2, you got me
        if (Math.pow(x1-x2, 2) + Math.pow(y1-y2, 2) <= radius)
        {
            hit(2);
            B.dead();
            StdAudio.playInBackground("hit.wav");
            if (isDead())
            {
                dead();
                score.plus(10);
            }
        }


    }

    public void Elimination(Laser C, Grader score)
    {
        double x1 = tx;
        double y1 = ty;
        double x2 = C.whereX();
        double y2 = C.whereY();
        double radius = 0.12;
        if (Math.abs(x1-x2) < radius && y1 > y2)
        {
            hit(1);
            StdAudio.playInBackground("hit.wav");
        }
        if (isDead())
        {
            dead();
            score.plus(10);
        }
        return;
    }
}
