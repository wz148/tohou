public class Bullet1{
    protected double db;
    protected double rx, ry; //position
    public boolean life = true; //sensor
    protected double bradius;//bullet radius
    protected double xspeed, yspeed;//bullet spped. Default to .05
    protected double ub, lb, rb; //upper,left,right bound
    public Bullet1(double rx0, double ry0, double ub0, double lb0, double rb0, double db0)
    {
        rx = rx0;
        ry = ry0;
        ub = ub0;
        lb = lb0;
        rb = rb0;
        db = db0;
        bradius = .05;
        yspeed = .08;
        xspeed = 0;
    }
    public Bullet1(double rx0, double ry0, double yspeed0)
    {
        rx = rx0;
        ry = ry0;
        yspeed = yspeed0;
        xspeed = 0;
        ub = 2.0;
        lb = -2.0;
        db = -2.0;
        rb = 2.0;
    }
    public void setSpeed(double xspeed0, double yspeed0)
    {
        xspeed = xspeed0;
        yspeed = yspeed0;
    }
    public void trace(double rx1, double ry1)
    {
        double prod = Math.sqrt(Math.pow(rx1-rx,2)+Math.pow(ry1-ry,2));
        double xs = 0.1*(rx1-rx)/prod;
        double ys = 0.1*(ry1-ry)/prod;
        setSpeed(xs, ys);
    }
    public void nextMove()
    {

        if (life){
            rx += xspeed;
            ry += yspeed;
            if (ry+ bradius >= ub || rx -bradius <= lb || rx + bradius >= rb || ry - bradius <= db)
            {
                dead();
                return;
            }
            else return;
        }
        else
        {
            rx = -2.0;
            ry = -3.0; //go to the gabage bin
        }
    }
    public void display()
    {
        if (life)
        {
            StdDraw.setPenColor(StdDraw.BLUE);
//            // Scale is 1.0
//            if (ry+ bradius >= ub || rx -bradius <= lb || rx + bradius >= rb || rx - bradius <= db)
//            {
//                dead();
//                return;
//            }
            StdDraw.filledCircle(rx, ry, bradius);
        }
        else return;
    }
    public void dead()
    {
        life = false;
    }
    public double whereX()
    {
        return rx;
    }
    public double whereY()
    {
        return ry;
    }
    public boolean readStat()
    {
        return life;
    }
}
