// package Tohou;

public class Plane {

    public static void main(String[] args)
    {
        
    }
    public double rx, ry;
    public double vx, vy;
    private final double ub, lb, rb, db, bradius;
    public boolean life = true;
    public Plane(double x0, double y0, double vx0, double vy0, double ub0, double lb0, double rb0, double db0, double br0)
    {
        rx = x0;
        ry = y0;
        vx = vx0;
        vy = vy0;
        ub = ub0;
        lb = lb0;
        rb = rb0;
        db = db0;
        bradius = br0;
    }
    public void Collition(Bullet1 B) // Actually I should read radius also, but I am too lazy =-)
    {
        double x1 = rx;
        double y1 = ry;
        double x2 = B.rx;
        double y2 = B.ry;
        double radius = 0.01; // 0.1*0.1, you got me
        if (Math.pow(x1-x2, 2) + Math.pow(y1-y2, 2) <= radius)
        {
            dead();
            B.dead();

        }
    }
    public void dead()
    {
        life = false;
        Main.lifelines--;
    }
    public boolean nextMove(boolean stage1, Bullet1[] bullets)
    {
        if (!life)
        {
            return false;
        }
        for (int i = 0; i < bullets.length; i++)
            if (bullets[i]!= null) Collition(bullets[i]);
        if (StdDraw.isKeyPressed(16)) // Shift
        {
            if (StdDraw.isKeyPressed(87) && ry+vy <= ub - bradius) ry += vy*6; //W
            else if (StdDraw.isKeyPressed(83) && ry-vy >= db + bradius) ry -= vy*6; //S
            if (StdDraw.isKeyPressed(65) && rx-vx >= lb + bradius) rx -= vx*6; //A
            else if (StdDraw.isKeyPressed(68) && rx+vx <= rb - bradius) rx += vx*6; //D
        }
        else
        {
            if (StdDraw.isKeyPressed(87) && ry+vy <= ub- bradius) ry += vy*3;
            else if (StdDraw.isKeyPressed(83) && ry-vy >= db+ bradius) ry -= vy*3;
            if (StdDraw.isKeyPressed(65) && rx-vx >= lb+ bradius) rx -= vx*3;
            else if (StdDraw.isKeyPressed(68) && rx+vx <= rb- bradius) rx += vx*3;
        }
        for (int i = 0; i<Main.bullet1s.length;i++)
        {
            if (Main.bullet1s[i]!=null) Collition(Main.bullet1s[i]);
        }
        return true;
    }
    public void display()
    {
        StdDraw.picture(rx, ry, "plane2.png", .7, .7);
        StdDraw.setPenColor(StdDraw.YELLOW);
        StdDraw.filledCircle(rx, ry, .05);
    }
    public double whereX()
    {
        return rx;
    }
    public double whereY()
    {
        return ry;
    }


    // public void flight()
    // {
        
    //     if (StdDraw.isKeyPressed(87)) ry += vy;
    //     else if (StdDraw.isKeyPressed(83)) ry -= vy;
    //     if (StdDraw.isKeyPressed(65)) rx -= vx;
    //     else if (StdDraw.isKeyPressed(68)) rx += vx;
    //     StdDraw.picture(rx, ry, "plane.png", .36, .36);
    //     // StdDraw.setPenColor(StdDraw.BLACK);
    //     // StdDraw.filledCircle(rx, ry, radius);
    //     StdDraw.enableDoubleBuffering();
    //     StdDraw.show();
    //     StdDraw.pause(10);
    //     // StdDraw.show(20);
    // }
    // public static void fight(double rx, double ry, double vx, double vy)
    // {
    //     // if (Math.abs(rx + vx) + radius > 1.0) vx=-vx;
    //     // if (Math.abs(ry + vy) + radius > 1.0) vy=-vy;
    //     // rx = rx + vx;
    //     // ry = ry + vy;
    //     if (StdDraw.isKeyPressed(87)) ry += vy;
    //     else if (StdDraw.isKeyPressed(83)) ry -= vy;
    //     if (StdDraw.isKeyPressed(65)) rx -= vx;
    //     else if (StdDraw.isKeyPressed(68)) rx += vx;
    //     StdDraw.picture(rx, ry, "plane.png", .36, .36);
    //     // StdDraw.setPenColor(StdDraw.BLACK);
    //     // StdDraw.filledCircle(rx, ry, radius);
    //     StdDraw.enableDoubleBuffering();
    //     StdDraw.show();
    //     StdDraw.pause(10);
    //     // StdDraw.show(20);
    // }
}
