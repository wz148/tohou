import java.awt.Color;
public class Background {
    public double yAxis,y,y1;
    public int timer,timer1;
    public Background(double y0)
    {
        yAxis = y0;
        y = y0;
        y1=y0;
        timer = 0;
        timer1 = 0;
    }
    public double[] xd = {-2.0,-2.0,3.0,3.0,2.0,2.0};
    public double[] yd = {-2.0,-3.0,-3.0,2.0,2.0,-2.0};
    public void display1(long a)
    {
        if(a<=3000) {
            y -= 8.0 / 3000.0;
            StdDraw.picture(0.0, y, "DKUMap1.png", 4.0, 12.0);
            StdDraw.setPenColor(StdDraw.WHITE);
            StdDraw.filledPolygon(xd, yd);
        }
        else StdDraw.picture(0.0,0.0,"DKUMap2.png",4.0,4.0);
    }
    public void display11(long a)
    {
        if (timer<=3000)
        {
            yAxis-=8.0/3000.0;
            StdDraw.picture(0.0, yAxis, "SAGMap.png", 4.0, 12.0);
            StdDraw.setPenColor(StdDraw.WHITE);
            StdDraw.filledPolygon(xd, yd);
        }
    }
    public void display111(long a)
    {
        if (timer1<=3000)
        {
            y1 -= 6.0/3000.0;
            StdDraw.picture(0.0, y1-2.0, "DKUMap1.png", 4.0, 12.0);
            StdDraw.setPenColor(StdDraw.WHITE);
            StdDraw.filledPolygon(xd, yd);
        }
    }
    public void display2()
    {
        StdDraw.setPenColor(StdDraw.WHITE);
        StdDraw.filledPolygon(xd, yd);
    }
}
