import java.util.*;

public class Grader {
    //testing only
    public double rx,ry,frame;
    public int index=0;
    public double[][] cordinates = new double[91][3];
    //testing only over
    private int stage;
    private int score;
    private int stagep =0;
    public double Lpower = 100;
    public int num =20;
    public int numres = 0;
    private final String[] grades= {"NC","CR","C","C+","B-","B","B+","A-","A","A+"}; // 8 Stages of Grades
    public Grader(int score0)
    {
        score = score0;
        stage = (int)score/100;
    }

    public void plus(int score0)
    {
        score += score0;
    }
    public String showGrade()
    {
        readGrade();
        if (stage < grades.length)
            return grades[stage];
        else return "A+";
    }
    public void displayBoss(BossLJ lj1){
        StdDraw.setPenColor(StdDraw.BLACK);
        double life= lj1.lifeline;
        double life0 = lj1.lifeline0;
        int cache = (int)((life/life0)*20)+1;
        num = cache<=20 ? cache : 20;
        System.out.println("num is "+ num+" life is "+lj1.lifeline);
        numres = 20-num;
        StdDraw.textLeft(-1.5, -2.5, "BOSS: ["+"- ".repeat(num)+"  ".repeat(numres)+"]");
//        StdDraw.text(0.0, -2.7, "Press Tab to Respawn");
    }
    public void displayBoss(BossLJ2 lj2){
        StdDraw.setPenColor(StdDraw.BLACK);
        double life= lj2.lifeline;
        double life0 = lj2.lifeline0;
        int cache = (int)((life/life0)*20)+1;
        num = cache<=20 ? cache : 20;
        System.out.println("num is "+ num+ " life is "+ lj2.lifeline);
        numres = 20-num;
        StdDraw.textLeft(-1.5, -2.5, "BOSS: ["+"- ".repeat(num)+"  ".repeat(numres)+"]");
//        StdDraw.text(0.0, -2.7, "Press Tab to Respawn");
    }
    public void display(Laser biu)
    {
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.text(2.5, 1.5, "SCORE: ");
        StdDraw.text(2.5, 1.3,  ""+score);
        StdDraw.text(2.5, 1.0, "CURRENT");
        StdDraw.text(2.5, 0.8,  "GRADE:");
        StdDraw.textLeft(2.0, 0.2,  "WASD: Move");
        StdDraw.textLeft(2.0, 0.0,  "Enter: Cannon");
        StdDraw.textLeft(2.0, -.2,  "Space: Laser");
        StdDraw.textLeft(2.0, -.4, "Laserpower: ");
        if (biu.life) {
            Lpower = (biu.lifeline / biu.lifeline0) * 100;
            StdDraw.textLeft(2.0, -.6, "" + Lpower+"%");
        }
        else StdDraw.text(2.5, -.6, "Empty");
        StdDraw.setPenColor(StdDraw.RED);
        StdDraw.textLeft(2.0, -.8, "Lifeline: "+ Main.lifelines);
        StdDraw.setPenColor(StdDraw.BLUE);
//        // Testing Usage
//        rx = StdDraw.mouseX();
//        ry = StdDraw.mouseY();
//        frame = Main.gframe + (2.0-ry)/8.0*3000;
//        StdDraw.text(2.5, -.8, "X:"+rx);
//        StdDraw.text(2.5, -1,"Y: "+ry);
//        if (StdDraw.isMousePressed())
//        {
//            cordinates[index%90][0]=rx;
//            cordinates[index%90][1]=ry;
//            cordinates[index%90][2]=frame;
//            index++;
//        }
        StdDraw.text(2.5, -1.2, "A GAME BY");
        StdDraw.text(2.5, -1.4, "Team_02");
        StdDraw.setPenColor(StdDraw.RED);
        StdDraw.text(2.5, 0.6, showGrade());
    }
    public void readGrade()
    {
        stage = (int)score/200;
        if (stage != stagep) StdAudio.playInBackground("UpgradeAu.wav");
        stagep = stage;
    }
    public void result()
    {
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.textLeft(-1.5, -2.3, "Your Final Grade is:          Thanks for Playing our game.");
        StdDraw.text(0.5, -2.5, "Team_02 sincerely hope that you can survive your Finals!");
        // StdDraw.text(2.5, 1.3,  "GRADE:");W
        StdDraw.setPenColor(StdDraw.RED);
        StdDraw.text(0.1, -2.3, showGrade());
    }
}
