public class Atkptn {
    //modify after having a timer in Main
    // x y are where the bullets come from
    // ptn1 60 degree arc in the front with a gap
    public static void ptn1(double x, double y,int timer){
        /*double precal1[][] = {{0.04000000000000001,-0.06928203230275509},  
        {0.03253893144606402,-0.07308363661140807},  
        {0.024721359549995797,-0.07608452130361229}, 
        {0.016632935265420756,-0.07825180805870445}, 
        {0.008362277061412278,-0.07956175162946187}, 
        {4.898587196589413E-18,-0.08},
        {-0.008362277061412285,-0.07956175162946187},
        {-0.01663293526542075,-0.07825180805870445}, 
        {-0.024721359549995787,-0.07608452130361229},
        {-0.03253893144606402,-0.07308363661140807},
        {-0.03999999999999998,-0.06928203230275509}};*/
        
        if (timer % 125 == 0){

            StdAudio.playInBackground("Shots.wav");

            int a = (int)(Math.random() * 11); 
            for(int i = 0; i < 11; i++){
                if(i != a){
                    Main.bullet1s[Main.patnum] = new Bullet1(x, y, Main.ub, Main.lb, Main.rb, Main.db);
                    Main.bullet1s[Main.patnum].setSpeed(Main.precal1[i][0], Main.precal1[i][1]);
                    Main.bullet1s[Main.patnum].nextMove();
                    Main.bullet1s[Main.patnum].display();
                    if(Main.patnum < Main.bullet1s.length - 1) Main.patnum += 1;
                    else Main.patnum = 0;
                }                
            }

        }
        else{
            for(int i = 0;i < Main.bullet1s.length;i++){
                if(Main.bullet1s[i] != null){
                    Main.bullet1s[i].nextMove();
                    Main.bullet1s[i].display();
                }
            }
        }
    }
    // each summoned drone lauch a tracing bullet every 100 frame
    // for simpilicity coordinates are taken automaticly
    public static void ptn2(Boss enemy1, Boss enemy2, Boss enemy3, Boss enemy4, int timer){
        if (timer % 100 == 0 && enemy1.life){
//            StdAudio.playInBackground("EnemyShot.wav");
            Main.bullet2s[Main.patnum2] = new Bullet1(enemy1.tx, enemy1.ty, Main.ub, Main.lb, Main.rb, Main.db);
            Main.bullet2s[Main.patnum2].trace(Main.rx, Main.ry);
            Main.bullet2s[Main.patnum2].nextMove();
            Main.bullet2s[Main.patnum2].display();
            if(Main.patnum2 < Main.bullet1s.length - 1) Main.patnum2 += 1;
            else Main.patnum2 = 0;

        }
        if (timer % 100 == 25 && enemy2.life){
//            StdAudio.playInBackground("EnemyShot.wav");
            Main.bullet2s[Main.patnum2] = new Bullet1(enemy2.tx, enemy2.ty, Main.ub, Main.lb, Main.rb, Main.db);
            Main.bullet2s[Main.patnum2].trace(Main.rx, Main.ry);
            Main.bullet2s[Main.patnum2].nextMove();
            Main.bullet2s[Main.patnum2].display();
            if(Main.patnum2 < Main.bullet1s.length - 1) Main.patnum2 += 1;
            else Main.patnum2 = 0;

        }
        if (timer % 100 == 50 && enemy3.life){
//            StdAudio.playInBackground("EnemyShot.wav");
            Main.bullet2s[Main.patnum2] = new Bullet1(enemy3.tx, enemy3.ty, Main.ub, Main.lb, Main.rb, Main.db);
            Main.bullet2s[Main.patnum2].trace(Main.rx, Main.ry);
            Main.bullet2s[Main.patnum2].nextMove();
            Main.bullet2s[Main.patnum2].display();
            if(Main.patnum2 < Main.bullet1s.length - 1) Main.patnum2 += 1;
            else Main.patnum2 = 0;

        }
        if (timer % 100 == 75 && enemy4.life){
//            StdAudio.playInBackground("EnemyShot.wav");
            Main.bullet2s[Main.patnum2] = new Bullet1(enemy4.tx, enemy4.ty, Main.ub, Main.lb, Main.rb, Main.db);
            Main.bullet2s[Main.patnum2].trace(Main.rx, Main.ry);
            Main.bullet2s[Main.patnum2].nextMove();
            Main.bullet2s[Main.patnum2].display();
            if(Main.patnum2 < Main.bullet1s.length - 1) Main.patnum2 += 1;
            else Main.patnum2 = 0;

        }

        if(timer % 25 != 0){// update every frame
            for(int i = 0;i < Main.bullet1s.length;i++){
                if(Main.bullet2s[i] != null){
                    Main.bullet2s[i].nextMove();
                    Main.bullet2s[i].display();
                }
            }
        }
    }

}