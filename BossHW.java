public class BossHW {
    public boolean life = true;
    public double tx, ty, size;
    public int timer;
    public int lifeline = 20;
    public int lifeline0 = 20;
    public String filename;
    public int index;
    public BossHW(double tx0, double ty0, double size0, String filename0, int index0)
    {
        tx = tx0;
        ty = ty0;
        size = size0;
        filename=filename0;
        index = index0;
//        lframe = gframe0;
    }
    public void moveTo(double x, double y)
    {
        tx = x;
        ty = y;
    }
    public void display()
    {
        if (!life)
        {
//            if (cnt)
            return;
        }
        else
        {
//            StdDraw.setPenColor(StdDraw.BLUE);
//            StdDraw.filledCircle(tx, ty, size);
            StdDraw.picture(tx, ty, filename, 0.2048*2, 0.1764*2);
            if (Main.ebullets[index]!=null) Main.ebullets[index].display();
        }
    }
    public void nextMove(Bullet[] bulletarr, Laser biu, Grader score)
    {
        if (life) {
            ty-=8.0/3000.0;
//            if (timer%75==0)
//            {
//                Main.ebullets[index]= new Bullet1(tx,ty,-0.08);
//                Main.ebullets[index].trace(Main.rx,Main.ry);
//            }
//            if(Main.ebullets[index]!=null){
//                Main.ebullets[index].nextMove();
//
//            }
            if (StdDraw.isKeyPressed(32) && biu.life) Elimination(biu, score);
            for (int j = 0; j < bulletarr.length; j++) {
                if (bulletarr[j] != null) Collition(bulletarr[j], score);
            }
            if (ty<-2.0) dead();
            timer++;
        }
        else return;
    }
    public boolean isDead()
    {
        return (lifeline<=0);
    }
    public void dead()
    {
        life = false;
        StdAudio.playInBackground("ExpAu.wav");
        // StdDraw.setPenColor(StdDraw.BLUE);
        // StdDraw.text(tx, ty, "+ "+ score0);
        tx = 3.0;
        ty = -3.0;
    }
    public void hit(int i)
    {
        lifeline-=i;
        StdAudio.playInBackground("hit.wav");
        return;
    }
    public double whereX()
    {
        return tx;
    }
    public double whereY()
    {
        return ty;
    }
    public void Collition(Bullet B, Grader score) // Actually I should read radius also, but I am too lazy =-)
    {
        double x1 = tx;
        double y1 = ty;
        double x2 = B.whereX();
        double y2 = B.whereY();
        double radius = 0.04; // 0.2*0.2, you got me
        if (Math.pow(x1-x2, 2) + Math.pow(y1-y2, 2) <= radius)
        {
            hit(2);
            B.dead();
            StdAudio.playInBackground("hit.wav");
            if (isDead())
            {
                dead();
                score.plus(25);
            }
        }


    }

    public void Elimination(Laser C, Grader score)
    {
        double x1 = tx;
        double y1 = ty;
        double x2 = C.whereX();
        double y2 = C.whereY();
        double radius = 0.12;
        if (Math.abs(x1-x2) < radius && y1 > y2)
        {
            hit(1);

        }
        if (isDead())
        {
            dead();
            score.plus(25);
        }
        return;
    }
}
