public class BossLJ2 {
    public boolean life = true;
    public double tx, ty, size;
    public int timer = 0;
    public int lifeline = 300;
    public int lifeline0 = 300;
    public Boss enemy1 = new Boss(-1.5,1.5,.5,1);
    public Boss enemy2 = new Boss(-1.0,1.0,.5,1);
    public Boss enemy3 = new Boss(1.0,1.0,.5,1);
    public Boss enemy4 = new Boss(1.5,1.5,.5,1);
//    public int index;
    public BossLJ2(double tx0, double ty0)
    {
        tx = tx0;
        ty = ty0;
//        size = size0;

//        lframe = gframe0;
    }
//    public void shot() // Imagine Dragon Respect!
//    {
//        if (Main.botbullets[index]==null) {
//            Main.botbullets[index] = new Bullet1(tx, ty, .08);
//            return;
//        }
//        else if (Main.botbullets[index].life)
//        {
//            Main.botbullets[index].nextMove();
//            return;
//        }
//        else Main.botbullets[index] = new Bullet1(tx, ty, .08);
//    }
    public void moveTo(double x, double y)
    {
        tx = x;
        ty = y;
    }
    public void display()
    {
        if (!life)
        {
//            if (cnt)
            return;
        }
        else
        {
//            StdDraw.setPenColor(StdDraw.BLUE);
//            StdDraw.filledCircle(tx, ty, size);
            StdDraw.picture(tx, ty, "LJDestroyer.png", 1.0*2, 0.5*2);
            enemy1.displayL();
            enemy2.displayL();
            enemy3.displayR();
            enemy4.displayR();
        }
    }
    public void nextMove(Bullet[] bulletarr, Laser biu, Grader score)
    {
        if (life) {
            timer++;
//            shot();
            Atkptn.ptn2(enemy1,enemy2,enemy3,enemy4,timer);
            Atkptn.ptn1(tx,ty,timer);
            if (StdDraw.isKeyPressed(32) && biu.life) Elimination(biu, score);
            for (int j = 0; j < bulletarr.length; j++) {
                if (bulletarr[j] != null) Collition(bulletarr[j], score);
            }
        }
        else return;
    }
    public boolean isDead()
    {
        return (lifeline<=0);
    }
    public void dead()
    {
        life = false;
//        if (Main.botbullets[index] != null) Main.botbullets[index].dead();
        StdAudio.playInBackground("ExpAu.wav");
        // StdDraw.setPenColor(StdDraw.BLUE);
        // StdDraw.text(tx, ty, "+ "+ score0);
        tx = 3.0;
        ty = -3.0;
    }
    public void hit(int i)
    {
        lifeline-=i;
        return;
    }
    public double whereX()
    {
        return tx;
    }
    public double whereY()
    {
        return ty;
    }
    public void Collition(Bullet B, Grader score) // Actually I should read radius also, but I am too lazy =-)
    {
        double x1 = tx;
        double y1 = ty;
        double x2 = B.whereX();
        double y2 = B.whereY();
        double radius = 0.04; // 0.2*0.2, you got me
        if (Math.pow(x1-x2, 2) + Math.pow(y1-y2, 2) <= radius)
        {
            hit(2);
            B.dead();
            StdAudio.playInBackground("hit.wav");
            if (isDead())
            {
                dead();
                score.plus(1000);
            }
        }


    }

    public void Elimination(Laser C, Grader score)
    {
        double x1 = tx;
        double y1 = ty;
        double x2 = C.whereX();
        double y2 = C.whereY();
        double radius = 0.3;
        if (Math.abs(x1-x2) < radius && y1 > y2)
        {
            hit(1);
            StdAudio.playInBackground("hit.wav");
        }
        if (isDead())
        {
            dead();
            score.plus(2000);
        }
        return;
    }
}
