import java.applet.Applet;
import java.applet.AudioClip;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
public class Sound {
    public AudioClip clip;
    public Sound(String pathname) throws MalformedURLException {
        File file = new File(pathname);
        URL url = null;
        if (file.canRead()) {url = file.toURI().toURL();}
        System.out.println(url);
        AudioClip clip = Applet.newAudioClip(url);
//        clip.play();
//        System.out.println("should've played by now");
    }
    public void play()
    {
        clip.play();
    }
    public void stop()
    {
        clip.stop();
    }
}
