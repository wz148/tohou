public class Story1 {
    private boolean s1,s2,s3,s4;
    public double[] xd = {-2.0,-2.0,2.0,2.0};
    public double[] yd = {-2.0,-3.0,-3.0,-2.0};
    public Story1()
    {
        s1=true;
    }
    public void nextmove()
    {
        if (s1) {
            if (StdDraw.hasNextKeyTyped()){
                if(StdDraw.nextKeyTyped()==8){
                    StdAudio.playInBackground("sans1.wav");
                    StdDraw.setPenColor(StdDraw.WHITE);
                    StdDraw.filledPolygon(xd, yd);
                    StdDraw.setPenColor(StdDraw.BLACK);
                    StdDraw.textLeft(-1.5, -2.5, "Prof. Long: ... by doing YOUR EASY HOMEWORKS?");
                    s1=false;
                    s2=true;
                }
            }
        }
        if (s2){
            if (StdDraw.hasNextKeyTyped()){
                if(StdDraw.nextKeyTyped()==8){
                    StdDraw.setPenColor(StdDraw.WHITE);
                    StdDraw.filledPolygon(xd, yd);
                    StdDraw.setPenColor(StdDraw.BLACK);
                    StdDraw.textLeft(-1.5, -2.5, "Prof. Long: You are now facing...");
                    StdDraw.picture(0.0,0.0,"LJFace.jpg",3.0,3.6);
                    s2=false;
                    s3=true;
                }
            }
        }
        if (s3){
            if (StdDraw.hasNextKeyTyped()){
                if(StdDraw.nextKeyTyped()==8){
                    StdAudio.playInBackground("sans1.wav");
                    StdDraw.setPenColor(StdDraw.WHITE);
                    StdDraw.filledPolygon(xd, yd);
                    StdDraw.setPenColor(StdDraw.BLACK);
                    StdDraw.textLeft(-1.5, -2.5, "Prof. Long: ... The REAL CHALLENGES.");
                    StdDraw.picture(0.0,0.0,"LJFace1.jpg",3.0,1.2*3);
                    s3=false;
                    s4=true;
                }
            }
        }
        if (s4){
            if (StdDraw.hasNextKeyTyped()){
                if(StdDraw.nextKeyTyped()==8){
                    Main.stage2 = false;
                    Main.stage3 = true;
                }
            }
        }
    }
    public void st1()
    {

    }
}
