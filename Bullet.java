

public class Bullet {
    protected double rx, ry; //position
    protected boolean life = true; //sensor
    protected double bradius;//bullet radius
    protected double xspeed, yspeed;//bullet spped. Default to .05
    protected double ub, lb, rb; //upper,left,right bound
    public Bullet()
    {
        // Default constructor
    }
    public Bullet(double rx0, double ry0, double ub0, double lb0, double rb0)
    {
        rx = rx0;
        ry = ry0 + 0.2;
        ub = ub0;
        lb = lb0;
        rb = rb0;
        bradius = .05;
        yspeed = .08;
        xspeed = 0;
    }
    public void setSpeed(double xspeed0, double yspeed0)
    {
        xspeed = xspeed0;
        yspeed = yspeed0;
    }
    public void nextMove()
    {

        if (life){
            rx += xspeed;
            ry += yspeed;
            if (ry+ bradius >= ub || rx -bradius <= lb || rx + bradius >= rb)
            {
                dead();
                return;
            }
            else return;
        }

        else 
        {
            rx = -2.0;
            ry = -3.0; //go to the gabage bin
        }
    }
    public void dead()
    {
        life = false;
    }
    public void display()
    {
        if (life)
        {
            StdDraw.setPenColor(StdDraw.RED);
            // Scale is 1.0
//            if (ry+ bradius >= ub || rx -bradius <= lb || rx + bradius >= rb)
//            {
//                dead();
//                return;
//            }
            StdDraw.filledCircle(rx, ry, bradius);
        }
        else return;
    }
    public double whereX()
    {
        return rx;
    }
    public double whereY()
    {
        return ry;
    }
    public boolean readStat()
    {
        return life;
    }
}
