public class Timer {
    private int tmax;    // Actual time is approximately t*20 milliseconds.
    private int t;
    public Timer(int t0, int tmax0)
    {
        t = t0;
        tmax = tmax0;
    }
    public void timeplus()
    {
        t+=1;
    }
}
