public class Laser {
    private final double ub; //upper bound
    private double x,y;
    public double lifeline = 300;
    public double lifeline0 = 300;
    public boolean life = true;
    public Laser()
    {
        ub = 2.0;
        x = 0;
        y = 0;
    }
    public Laser(double x0, double y0, double ub0)
    {
        ub = ub0;
        x = x0;
        y = y0+0.4;
    }
    public void moveTo(double x0, double y0)
    {
        x = x0;
        y = y0+0.4;
    }
    public double whereX()
    {
        return x;
    }
    public double whereY()
    {
        return y;
    }
    public void isDead()
    {
        if (lifeline<=0)
        {
            life = false;
            return;
        }
    }
    public void display()
    {
        if (life) {
            if (StdDraw.isKeyPressed(32)) {
                lifeline--;
            }
            StdDraw.setPenColor(StdDraw.GREEN);
            StdDraw.setPenRadius(0.1);
            StdDraw.line(x, y, x, ub);
            isDead();
        }
        else return;
    }


    // public static void main(String[] args)
    // {
    //     .display();
    // }
    // Test if git works
    // Test if push works
}
