//import javax.sound.sampled.AudioInputStream;
//import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import java.awt.*;
// package Tohou;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.*;
public class Main{
        // Old fashioned objects
//        public static Font title = new Font("Arial", Font.PLAIN, 32);
        public static int bulletnum = 0;
        public static int ebulletnum = 0;
        public static boolean sb = false;
        public static double bradius = .05;
        public static double rx = 0.0, ry = -1.5;
        public static double vx = .015, vy = .023;
        public static double lb =-2.0, rb = 2.0;
        public static double db = -2.0, ub = 2.0;
        public static boolean stage1 = true;
        public static boolean stage0 = true;
        public static boolean stage2;
        public static boolean stage3;
        public static boolean stage4;
        public static boolean stage5;
        public static long delt,t0;
        public static long[] times = new long[100];
        public static int ti = 0;
        public static long gframe = 0;
        public static int lifelines = 10;
        public static Font big = new Font("Arial",Font.BOLD,32);

    // New objects
        // Player
    public static Plane plane = new Plane(rx, ry, vx, vy, ub,lb,rb,db,bradius);
    public static Background bg = new Background(4.0);
        // Weapons
    public static Bullet [] bulletarr = new Bullet[60];
    public static Laser biu = new Laser();
    public static Bullet1 [] ebullets = new Bullet1[12];
    public static Bullet1 [] botbullets = new Bullet1[9];
    // David's work
    public static Bullet1 [] bullet1s = new Bullet1[30];
    public static Bullet1 [] bullet2s = new Bullet1[30];
    public static int patnum = 0;
    public static int patnum2 = 0;
    public static final double precal1[][] = {{0.04000000000000001,-0.06928203230275509},
            {0.03253893144606402,-0.07308363661140807},
            {0.024721359549995797,-0.07608452130361229},
            {0.016632935265420756,-0.07825180805870445},
            {0.008362277061412278,-0.07956175162946187},
            {4.898587196589413E-18,-0.08},
            {-0.008362277061412285,-0.07956175162946187},
            {-0.01663293526542075,-0.07825180805870445},
            {-0.024721359549995787,-0.07608452130361229},
            {-0.03253893144606402,-0.07308363661140807},
            {-0.03999999999999998,-0.06928203230275509}};
        // Enemies
    public static Boss [] bossarr = new Boss[8];
    public static Grader score = new Grader(0);
    public static BossHW [] homeworks = new BossHW [12];
//    public static Sound bgm0;
    public static Clip bgm0,bgm1,bgm2, bgm4,BadApple;
    public static AudioInputStream audioInputStream0,audioInputStream1,audioInputStream2, audioInputStream3,audioInputStream4;
    static   {
        try {
            audioInputStream0 = AudioSystem.getAudioInputStream(new File("C:/Users/Weicheng Zheng/Desktop/CS201/Games_I_love/STG1DKU/BGM0.wav").getAbsoluteFile());
            bgm0 = AudioSystem.getClip();
        } catch(Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }
    static   {
        try {
            audioInputStream1 = AudioSystem.getAudioInputStream(new File("C:/Users/Weicheng Zheng/Desktop/CS201/Games_I_love/STG1DKU/BGM1.wav").getAbsoluteFile());
            bgm1 = AudioSystem.getClip();
        } catch(Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }
    static   {
        try {
            audioInputStream2 = AudioSystem.getAudioInputStream(new File("C:/Users/Weicheng Zheng/Desktop/CS201/Games_I_love/STG1DKU/BGM3.wav").getAbsoluteFile());
            bgm2 = AudioSystem.getClip();
        } catch(Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }
    static   {
        try {
            audioInputStream3 = AudioSystem.getAudioInputStream(new File("C:/Users/Weicheng Zheng/Desktop/CS201/Games_I_love/STG1DKU/BadApple.wav").getAbsoluteFile());
            BadApple = AudioSystem.getClip();
        } catch(Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }
    static   {
        try {
            audioInputStream4 = AudioSystem.getAudioInputStream(new File("C:/Users/Weicheng Zheng/Desktop/CS201/Games_I_love/STG1DKU/BGM4.wav").getAbsoluteFile());
            bgm4 = AudioSystem.getClip();
        } catch(Exception ex) {
            System.out.println("Error with playing sound.");
            ex.printStackTrace();
        }
    }
//    static {
//        try {
//            bgm0 = new Sound("C:/Users/Weicheng Zheng/Desktop/CS201/Games_I_love/STG1DKU/BGM0.wav");
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
//    }

//    public static Sound bgm1;
//
//    static {
//        try {
//            bgm1 = new Sound("C:\\Users\\Weicheng/Zheng\\Desktop\\CS201\\Games_I_love\\STG1DKU\\BGM1.wav");
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//        }
//    }

    // public static double lifeline = 100.0;
    public static void main(String args[]) throws LineUnavailableException, IOException {
        NewEnemy1();
        for (int i = 0; i < bossarr.length; i++)
        {
            bossarr[i].dead();
        }
        StdDraw.setXscale(lb, rb+1);
        StdDraw.setYscale(db-1, ub);
        StdDraw.enableDoubleBuffering();
        StdDraw.textLeft(-1.5,-2.8,"Team_02 Presents");
        StdDraw.setFont(big);
        StdDraw.textLeft(-1.5,-2.5,"Press Enter to Start");
        StdDraw.setFont();
        BadApple.open(audioInputStream3);
        BadApple.start();
        StdDraw.setPenColor(StdDraw.WHITE);
        while (stage0)
        {
            StdDraw.filledSquare(0.0, 0.0, rb);
            StdDraw.picture(0.0,0.0,"DanceL1.jpg");
            StdDraw.show();
            StdDraw.pause(250);
            StdDraw.filledSquare(0.0, 0.0, rb);
            StdDraw.picture(0.0,0.0,"DanceL2.jpg");
            StdDraw.show();
            StdDraw.pause(250);
            if (StdDraw.hasNextKeyTyped())
                if(StdDraw.nextKeyTyped()==10) stage0 = false;
        }
        BadApple.close();
        StdAudio.playInBackground("ButtonAu.wav");
//        StdDraw.pause(100);
        ebullets[0] = new Bullet1(-0.5, 2.0, ub, lb, rb, db);
        ebullets[1] = new Bullet1(0.5, 2.0, ub, lb, rb, db);
//        Background bg = new Background(4.0);
//        StdAudio.playInBackground("BGM0.wav");
//        bgm0.play();
        bgm0.open(audioInputStream0);
        bgm0.start();
        while (stage1) {
            t0 = System.currentTimeMillis();
            //Background
//            StdDraw.setPenColor(StdDraw.WHITE);
//            StdDraw.filledSquare(0.0, 0.0, rb + 1);
//            StdDraw.setPenColor(StdDraw.LIGHT_GRAY);
//            StdDraw.filledSquare(0.0, 0.0, rb);
            bg.display1(gframe);
            //
            NewHomework();
            while (!plane.life && stage1)
            {
//                lifelines--;
                if (lifelines >=0) {
                    clearBullets();
                    relife();
                    StdDraw.show();
                    StdDraw.pause(20);
                }
                else stage1=false;
            }
            if (plane.life) {
                // // Plane Module
                stage1 = plane.nextMove(stage1, ebullets);
                plane.display();
                rx = plane.whereX();
                ry = plane.whereY();

//                if (!ebullets[1].life) {
//                    ebullets[1] = new Bullet1(0.5, 1.5, ub, lb, rb, db);
//                    ebullets[1].trace(rx, ry);
//                    //                double prod = Math.sqrt(Math.pow(rx+0.5,2)+Math.pow(ry-1.5,2));
//                    //                double xs = 0.1*(rx+0.5)/prod;
//                    //                double ys = 0.1*(ry-1.5)/prod;
//                    //                ebullets[0].setSpeed(xs, ys);
//                }
//                if (!ebullets[0].life) {
//                    ebullets[0] = new Bullet1(-0.5, 1.5, ub, lb, rb, db);
//                    ebullets[0].trace(rx, ry);
//                    //                double prod = Math.sqrt(Math.pow(rx+0.5,2)+Math.pow(ry-1.5,2));
//                    //                double xs = 0.1*(rx+0.5)/prod;
//                    //                double ys = 0.1*(ry-1.5)/prod;
//                    //                ebullets[0].setSpeed(xs, ys);
//                }
//                //            ebulletnum++;
//                ebullets[0].nextMove();
//                ebullets[0].display();
//                ebullets[1].nextMove();
//                ebullets[1].display();
//
//                for (int i = 0; i < ebullets.length; i++)
//                    if (ebullets[i] != null) ebullets[i].nextMove();
//                for (int i = 0; i < ebullets.length; i++)
//                    if (ebullets[i] != null) ebullets[i].display();
                // Bullet Module
                if (StdDraw.hasNextKeyTyped())
                    if (StdDraw.nextKeyTyped() == 10) //Enter
                    {
                        StdAudio.playInBackground("BulletAu.wav");
                        bulletarr[bulletnum] = new Bullet(rx, ry, ub, lb, rb); //Generate new bullet
                        bulletarr[bulletnum + 1] = new Bullet(rx, ry, ub, lb, rb);
                        bulletarr[bulletnum + 1].setSpeed(-.02, .08);
                        bulletarr[bulletnum + 2] = new Bullet(rx, ry, ub, lb, rb);
                        bulletarr[bulletnum + 2].setSpeed(.02, .08);
                        // Bullet a = bulletarr[bulletnum];
                        bulletnum += 3;
                        if (bulletnum >= bulletarr.length) bulletnum = 0;
                    }
                //                else if (StdDraw.nextKeyTyped()==27) stage1 = false; //Escape
                //                else continue;
                for (int i = 0; i < bulletarr.length; i++)
                    if (bulletarr[i] != null) bulletarr[i].nextMove();
                for (int i = 0; i < bulletarr.length; i++)
                    if (bulletarr[i] != null) bulletarr[i].display();
            }
            // Laser Module
            if (StdDraw.isKeyPressed(32)) // Space
            {
//                StdAudio.playInBackground("LaserAu.wav");
                biu.moveTo(rx,ry);
                biu.display();
            }
            // Enemy Setting
            // Generate new enemies
            if (StdDraw.isKeyPressed(9)) NewEnemy2(); //Backspace
            // Enemies move & Collitions detection
            for (int i = 0; i < bossarr.length; i++) {
                if (bossarr[i]!= null) bossarr[i].nextMove(bulletarr, biu, score);
            }
            for (int j = 0; j < homeworks.length; j++)
            {
                if (homeworks[j]!= null) homeworks[j].nextMove(bulletarr, biu, score);
            }
            for (int i = 0; i < botbullets.length; i++)
            {
                if (botbullets[i]!=null) botbullets[i].nextMove();
            }

            // Enemies display
            for (int i = 0; i < bossarr.length; i++) {
                if (bossarr[i]!= null) bossarr[i].display();
            }
            for (int j = 0; j < homeworks.length; j++){
                if (homeworks[j]!= null) homeworks[j].display();
            }
            for (int i = 0; i < botbullets.length; i++) {
                if (botbullets[i] != null) botbullets[i].display();
            }
            // UI Display 虽然我们并没有UI..
            bg.display2();
            // Grade Display
            score.display(biu);

//            // Music Player
//            if (StdDraw.isKeyPressed(8)) StdAudio.playInBackground("BGM0.wav"); //BackSpace
            // End Code
            if (StdDraw.isKeyPressed(27))//Escape
            {
                stage1 = false;
                stage2 = true;
            }

            // Show Maker
            StdDraw.show();
            delt = System.currentTimeMillis() - t0;
            if (delt <= 20) {
                StdDraw.pause(20 - (int) delt);
            }
            else StdDraw.pause(0);
            times[ti%10] = delt;
            ti++;
            gframe+=1;
            System.out.println(gframe);
        }
//        StdAudio.close();
//        bgm0.stop();
        bgm0.close();
        if(stage2&&!stage1) {
            Story1 chat1 = new Story1();
            StdDraw.picture(0.0, 0.0, "DKUMap2.png", 4.0, 4.0);
            StdDraw.setPenColor(StdDraw.WHITE);
            double[] xd = {-2.0, -2.0, 2.0, 2.0};
            double[] yd = {-2.0, -3.0, -3.0, -2.0};
            StdDraw.filledPolygon(xd, yd);
            StdDraw.setPenColor(StdDraw.BLACK);
            StdAudio.playInBackground("sans1.wav");
            StdDraw.textLeft(-1.5, -2.5, "Prof. Long: Do you think you can get an A...");
            while(!stage1 && stage2)
            {
                chat1.nextmove();
                StdDraw.show();
                StdDraw.pause(20);
            }
            warningM();
        }

        BossLJ lj1 = new BossLJ(0.0,1.5);
//        StdAudio.playInBackground("BGM1.wav");
//        bgm1.play();
        if (!stage2 && stage3) {
            clearBullets();
            bgm1.open(audioInputStream1);
            bgm1.start();
        }
        while (!stage2 && stage3)
        {
            if (!lj1.life) {
                stage3 = false;
                stage4 = true;
                break;
            }
            BossFight1(lj1);
        }
        bgm1.close();
        if(!stage3 && stage4){

            bgm2.open(audioInputStream2);
            bgm2.start();
//        bgm4.open(audioInputStream4);
//        bgm4.start();
            Story2 chat2 = new Story2();
            StdDraw.picture(0.0,0.0,"DKUMap2.png", 4.0,4.0);
            StdDraw.setPenColor(StdDraw.WHITE);
            double[] xd = {-2.0,-2.0,2.0,2.0};
            double[] yd = {-2.0,-3.0,-3.0,-2.0};
            StdDraw.filledPolygon(xd, yd);
            StdDraw.setPenColor(StdDraw.BLACK);
            StdAudio.playInBackground("sans1.wav");
            StdDraw.textLeft(-1.5, -2.5, "Prof. Long: Well done. You survived it.");
//        StdDraw.pause(1000);
            while(!stage3 && stage4)
            {
                chat2.nextmove();
                StdDraw.show();
                StdDraw.pause(20);
            }
            warningF();
        }

        BossLJ2 lj2 = new BossLJ2(0.0,1.5);
        clearBullets();
        while (stage5)
        {
            if (!lj2.life) {
                stage5 = false;
                break;
            }
            BossFight2(lj2);
        }
        // Show results
//        bgm1.stop();
        bgm2.close();
//        bgm4.close();
        bg.display2();
        score.result();
        StdDraw.show();
        StdAudio.playInBackground("Victory.wav");
//        long ave= 0;
//        for (int i = 0; i < times.length; i++)
//        {
//            ave+=times[i];
//        }
//        System.out.println("Average time is: "+ave/100);
        System.out.println("yAxis = "+bg.y);
        double[][] cors = score.cordinates;
        for (int i=0; i<cors.length;i++) {
            if (cors[i] != null) {
                System.out.println(Arrays.toString(cors[i]));
            }
        }
//        StdAudio.close();
    }

    // Boss Fight Stage 1
    public static void BossFight1(BossLJ lj1)
    {
        t0 = System.currentTimeMillis();
        //Background
//            StdDraw.setPenColor(StdDraw.WHITE);
//            StdDraw.filledSquare(0.0, 0.0, rb + 1);
//            StdDraw.setPenColor(StdDraw.LIGHT_GRAY);
//            StdDraw.filledSquare(0.0, 0.0, rb);
        bg.display11(gframe);
        // BOSSLJ module
        lj1.nextMove(bulletarr,biu,score);
        lj1.display();
        // Plane module
        while (!plane.life && stage3)
        {
//            lifelines--;
            if (lifelines >=0) {
                clearBullets();
                relife();
                StdDraw.show();
                StdDraw.pause(20);
//                break;
            }
            else
            {
                stage3 = false;
//                stage4 = true;
//                break;
            }
        }
        stage3 = plane.nextMove(stage3, ebullets);
        plane.display();
        rx = plane.whereX();
        ry = plane.whereY();
        // Bullet Module
        if (StdDraw.hasNextKeyTyped())
            if (StdDraw.nextKeyTyped() == 10) //Enter
            {
                StdAudio.playInBackground("BulletAu.wav");
                bulletarr[bulletnum] = new Bullet(rx, ry, ub, lb, rb); //Generate new bullet
                bulletarr[bulletnum + 1] = new Bullet(rx, ry, ub, lb, rb);
                bulletarr[bulletnum + 1].setSpeed(-.02, .08);
                bulletarr[bulletnum + 2] = new Bullet(rx, ry, ub, lb, rb);
                bulletarr[bulletnum + 2].setSpeed(.02, .08);
                // Bullet a = bulletarr[bulletnum];
                bulletnum += 3;
                if (bulletnum >= bulletarr.length) bulletnum = 0;
            }
        //                else if (StdDraw.nextKeyTyped()==27) stage1 = false; //Escape
        //                else continue;
        for (int i = 0; i < bulletarr.length; i++)
            if (bulletarr[i] != null) bulletarr[i].nextMove();
        for (int i = 0; i < bulletarr.length; i++)
            if (bulletarr[i] != null) bulletarr[i].display();
        // Laser Module
        if (StdDraw.isKeyPressed(32)) // Space
        {
//                StdAudio.playInBackground("LaserAu.wav");
            biu.moveTo(rx,ry);
            biu.display();
        }
        // UI Display 虽然我们并没有UI..
        bg.display2();
        // Grade Display
        score.display(biu);
        score.displayBoss(lj1);
        // End Code
        if (StdDraw.isKeyPressed(27))//Escape
        {
            stage3 = false;
            stage4 = true;
//                stage0 = true;
            StdDraw.pause(500);
        }
        // Show Maker
        StdDraw.show();
        delt = System.currentTimeMillis() - t0;
        if (delt <= 20) {
            StdDraw.pause(20 - (int) delt);
        }
        else StdDraw.pause(0);
        times[ti%10] = delt;
        ti++;
        gframe+=1;
        System.out.println(gframe);
    }

    // Bost Fight Stage 2
    public static void BossFight2(BossLJ2 lj2) {
        t0 = System.currentTimeMillis();
        //Background
//            StdDraw.setPenColor(StdDraw.WHITE);
//            StdDraw.filledSquare(0.0, 0.0, rb + 1);
//            StdDraw.setPenColor(StdDraw.LIGHT_GRAY);
//            StdDraw.filledSquare(0.0, 0.0, rb);
        bg.display111(gframe);
        // BOSSLJ module
        lj2.nextMove(bulletarr, biu, score);
        lj2.display();
        // Plane module
        while (!plane.life && stage5) {
//            lifelines--;
            if (lifelines >= 0) {
                clearBullets();
                relife2();
                StdDraw.show();
                StdDraw.pause(20);
//                break;
            }
            else
            {
                stage5 = false;
//                break;
            }
        }
        stage4 = plane.nextMove(stage4, ebullets);
        plane.display();
        rx = plane.whereX();
        ry = plane.whereY();
        // Bullet Module
        if (StdDraw.hasNextKeyTyped())
            if (StdDraw.nextKeyTyped() == 10) //Enter
            {
                StdAudio.playInBackground("BulletAu.wav");
                bulletarr[bulletnum] = new Bullet(rx, ry, ub, lb, rb); //Generate new bullet
                bulletarr[bulletnum + 1] = new Bullet(rx, ry, ub, lb, rb);
                bulletarr[bulletnum + 1].setSpeed(-.02, .08);
                bulletarr[bulletnum + 2] = new Bullet(rx, ry, ub, lb, rb);
                bulletarr[bulletnum + 2].setSpeed(.02, .08);
                // Bullet a = bulletarr[bulletnum];
                bulletnum += 3;
                if (bulletnum >= bulletarr.length) bulletnum = 0;
            }
        //                else if (StdDraw.nextKeyTyped()==27) stage1 = false; //Escape
        //                else continue;
        for (int i = 0; i < bulletarr.length; i++)
            if (bulletarr[i] != null) bulletarr[i].nextMove();
        for (int i = 0; i < bulletarr.length; i++)
            if (bulletarr[i] != null) bulletarr[i].display();
        // Laser Module
        if (StdDraw.isKeyPressed(32)) // Space
        {
//                StdAudio.playInBackground("LaserAu.wav");
            biu.moveTo(rx, ry);
            biu.display();
        }
        // UI Display 虽然我们并没有UI..
        bg.display2();
        // Grade Display
        score.display(biu);
        score.displayBoss(lj2);
        // End Code
//        if (StdDraw.isKeyPressed(27))//Escape
//        {
//            stage4 = false;
////                stage0 = true;
//        }
        // Show Maker
        StdDraw.show();
        delt = System.currentTimeMillis() - t0;
        if (delt <= 20) {
            StdDraw.pause(20 - (int) delt);
        } else StdDraw.pause(0);
        times[ti % 10] = delt;
        ti++;
        gframe += 1;
        System.out.println(gframe);
    }

    public static void NewEnemy1()
    {
        double ty = 1.6;
        double tx = -1.7;
        for (int i = 0; i < bulletarr.length; i++)
        {
            if (bulletarr[i] != null) bulletarr[i].dead();
        }
        for (int i = 0; i < 4; i++)
        {
            bossarr[i] = new Boss(tx, ty, 0.2, i);
            tx += 0.45;
            ty -= 0.45;
        }
        tx += 0.2;
        ty += 0.45;
        for (int j = 4; j < bossarr.length; j++)
        {
            bossarr[j] = new Boss(tx, ty, 0.2, j);
            tx +=0.45;
            ty += 0.45;
        }
    }
    public static void NewEnemy2()
    {
        double ty = 1.4;
        double tx = -1.2;
        for (int i = 0; i < bulletarr.length; i++)
        {
            if (bulletarr[i] != null) bulletarr[i].dead();
        }
        for (int i = 0; i < 5; i++)
        {
            bossarr[i] = new Boss(tx, ty, 0.2, i);
            tx += 0.6;
        }
        tx = -0.6;
        ty = 0.5;
        for (int j = 5; j < bossarr.length; j++)
        {
            bossarr[j] = new Boss(tx, ty, 0.2, j);
            tx +=0.6;
        }
    }

    public static void NewHomework() {
        if (gframe == 2) homeworks[0]=new BossHW(-0.16,1.0,0.5,"hw1.png",0);
        if (gframe == 300) NewEnemy1();
        if (gframe == 725) homeworks[1]=new BossHW(1.73,2.0,0.5,"hw2.png",1);
        if (gframe == 950) homeworks[2]=new BossHW(-0.41,2.0,0.5,"hw3.png",2);
        if (gframe == 1225) homeworks[3]=new BossHW(-1.54,2.0,0.5,"hw4.png",3);
        if (gframe == 1425) homeworks[4]=new BossHW(0.56,2.0,0.5,"hw5.png",4);
        if (gframe == 1725) homeworks[5]=new BossHW(-0.44,2.0,0.5,"hw6.png",5);
        if (gframe == 1705) homeworks[6]=new BossHW(1.26,2.0,0.5,"hw7.png",6);
        if (gframe == 1900) NewEnemy2();
        if (gframe == 2100) homeworks[7]=new BossHW(-0.55,2.0,0.5,"hw8.png",7);
        if (gframe ==  2175) homeworks[8]=new BossHW(0.76,2.0,0.5,"hw9.png",8);
        if (gframe == 2450) NewEnemy1();
        if (gframe == 2650) homeworks[9]=new BossHW(0.87,2.0,0.5,"hw10.png",9);
        if (gframe == 2700) homeworks[10]=new BossHW(-1.74,2.0,0.5,"hw_pro1.png",10);
    }

    public static void relife()
    {
        double[] xd = {-2.0,-2.0,2.0,2.0};
        double[] yd = {-2.0,-3.0,-3.0,-2.0};
        double[] xd1 = {-2.0,-2.0,2.0,2.0};
        double[] yd1 = {1.0,-1.0,-1.0,1.0};
        StdDraw.setPenColor(StdDraw.WHITE);
        StdDraw.filledPolygon(xd, yd);
        StdDraw.filledPolygon(xd1,yd1);
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.textLeft(-1.5, -2.5, "Sichang: You cannot die now! Let me help you!");
        StdDraw.text(0.0, -.6, "Hint: Only Attack the Head");
        StdDraw.setFont(big);
        StdDraw.text(0.0,0.0,"Press Tab to Respawn");
        StdDraw.setFont();
        if (StdDraw.isKeyPressed(9))
        {
            plane.life = true;
        }
    }
    public static void relife2()
    {
        double[] xd = {-2.0,-2.0,2.0,2.0};
        double[] yd = {-2.0,-3.0,-3.0,-2.0};
        double[] xd1 = {-2.0,-2.0,2.0,2.0};
        double[] yd1 = {1.0,-1.0,-1.0,1.0};
        StdDraw.setPenColor(StdDraw.WHITE);
        StdDraw.filledPolygon(xd, yd);
        StdDraw.filledPolygon(xd1,yd1);
        StdDraw.setPenColor(StdDraw.BLACK);
        StdDraw.textLeft(-1.5, -2.5, "Sichang He: You cannot die now! Let me help you!");
        StdDraw.text(0.0, -0.6, "Hint: Move per Quarter-Second");
        StdDraw.setFont(big);
        StdDraw.text(0.0,0.0,"Press Tab to Respawn");
        StdDraw.setFont();
        if (StdDraw.isKeyPressed(9))
        {
            plane.life = true;
        }
    }
    public static void warningM()
    {
        double[] xd1 = {-2.0,-2.0,2.0,2.0};
        double[] yd1 = {1.0,-1.0,-1.0,1.0};
        StdDraw.setFont(big);
        StdAudio.playInBackground("Warning.wav");
        for (int i=0; i<10;i++) {
            StdDraw.setPenColor(StdDraw.RED);
            StdDraw.filledPolygon(xd1, yd1);
            StdDraw.setPenColor(StdDraw.WHITE);
            StdDraw.text(0.0, 0.0, "!!!MIDTERM WARNING!!!");
            StdDraw.show();
            StdDraw.pause(250);
            StdDraw.setPenColor(StdDraw.WHITE);
            StdDraw.filledPolygon(xd1, yd1);
            StdDraw.setPenColor(StdDraw.RED);
            StdDraw.text(0.0, 0.0, "!!!MIDTERM WARNING!!!");
            StdDraw.show();
            StdDraw.pause(250);
        }
        StdDraw.setFont();
    }
    public static void warningF()
    {
        double[] xd1 = {-2.0,-2.0,2.0,2.0};
        double[] yd1 = {1.0,-1.0,-1.0,1.0};
        StdDraw.setFont(big);
        for (int i=0; i<5;i++) {
            StdDraw.setPenColor(StdDraw.RED);
            StdDraw.filledPolygon(xd1, yd1);
            StdDraw.setPenColor(StdDraw.WHITE);
            StdDraw.text(0.0, 0.0, "!!!FINAL WARNING!!!");
            StdDraw.show();
            StdDraw.pause(250);
            StdDraw.setPenColor(StdDraw.WHITE);
            StdDraw.filledPolygon(xd1, yd1);
            StdDraw.setPenColor(StdDraw.RED);
            StdDraw.text(0.0, 0.0, "!!!FINAL WARNING!!!");
            StdDraw.show();
            StdDraw.pause(250);
        }
        StdDraw.setFont();
    }
    public static void clearBullets()
    {
        for (Bullet a: bulletarr)
        {
            if (a!=null) a.dead();
        }
        for (Bullet1 b : bullet1s)
        {
            if (b!=null) b.dead();
        }
        for (Bullet1 c : bullet2s)
        {
            if (c!=null) c.dead();
        }
    }
    
    // public static void Collition(Boss A, Bullet B) // Actually I should read radius also, but I am too lazy =-)
    // {
    //     double x1 = A.whereX();
    //     double y1 = A.whereY();
    //     double x2 = B.whereX();
    //     double y2 = B.whereY();
    //     double radius = 0.04; // 0.2*0.2, you got me
    //     if (Math.pow(x1-x2, 2) + Math.pow(y1-y2, 2) <= radius)
    //     {
    //         A.dead();
    //         B.dead();
    //         score.plus(50);
    //     }

    // }

    // public static void Elimination(Boss A, Laser C)
    // {
    //     double x1 = A.whereX();
    //     double y1 = A.whereY();
    //     double x2 = C.whereX();
    //     double y2 = C.whereY();
    //     double radius = 0.12;
    //     if (Math.abs(x1-x2) < radius && y1 > y2)
    //     {
    //         A.dead();
    //         score.plus(25);
    //     }
    //     return;
    // }






    // public static void ammo(boolean sb)
    // {
    //     // Font font = new Font("Arial", 60);
    //     // StdDraw.setFont(font);
    //     StdDraw.setPenColor(StdDraw.BLACK);
    //     StdDraw.text(1.7, -1.50, "AMMO");
    //     if (sb)
    //     {
    //         StdDraw.setPenColor(StdDraw.BLACK);
    //         StdDraw.square(1.7, -1.7, 0.1);
    //         StdDraw.setPenColor(StdDraw.BLACK);
    //         StdDraw.filledCircle(1.7, -1.7, 0.1);
    //     }
    //     else
    //     {
    //         StdDraw.setPenColor(StdDraw.BLACK);
    //         StdDraw.square(1.7, -1.7, 0.1);
    //         StdDraw.setPenColor(StdDraw.RED);
    //         StdDraw.filledCircle(1.7, -1.7, 0.1);
    //     }
    // }
    
    // public static void bullet(double rx, double ry)
    // {
    //     StdDraw.setPenColor(StdDraw.RED);
    //     StdDraw.filledCircle(rx, ry+0.1, .05);
    //     bulletarr[0] = rx;
    //     bulletarr[1] = ry+0.1;
    //     sb = true;
    // }

    // public static void bulleton(double speed)
    // {
    //     StdDraw.setPenColor(StdDraw.RED);
    //     bulletarr[1] += speed;
    //     // Scale is 1.0
    //     if (bulletarr[1]+ bradius >= ub) 
    //     {
    //         sb = false;
    //         return;
    //     }
    //     else StdDraw.filledCircle(bulletarr[0], bulletarr[1], bradius);
    // }
}

//Class Bullet
// public class Bullet{
//     public static void bullet(double rx, double ry)
// {
//     StdDraw.setPenColor(StdDraw.RED);
//     StdDraw.filledCircle(rx, ry+0.1, .05);
//     bulletarr[0] = rx;
//     bulletarr[1] = ry+0.1;
//     sb = true;
// }

// public static void bulleton(double speed)
// {
//     StdDraw.setPenColor(StdDraw.RED);
//     bulletarr[1] += speed;
//     // Scale is 1.0
//     if (bulletarr[1]+ bradius >= ub) 
//     {
//         sb = false;
//         return;
//     }
//     else StdDraw.filledCircle(bulletarr[0], bulletarr[1], bradius);
// }
