public class Laser2 {
    private final double db; //upper bound
    private double x,y;
    public Laser2(double x0, double y0, double db0)
    {
        db = db0;
        x = x0;
        y = y0+0.4;
    }
    public Laser2(double x0, double y0)
    {
        db = -2.0;
        x = x0;
        y = y0;
    }
    public double whereX()
    {
        return x;
    }
    public double whereY()
    {
        return y;
    }
    public void display()
    {
        StdDraw.setPenColor(StdDraw.GREEN);
        StdDraw.setPenRadius(0.1);
        StdDraw.line(x, y, x, db);
    }
}
